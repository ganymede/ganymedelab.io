---
title: Flower Project
date: 2020-07-14
tags: [IT, Machine Learning, Neural Network, Image Recognition]
---

I have created the following project for the course Data Science by [Dr. Karsten Tolle](https://www.dbis.cs.uni-frankfurt.de/index.php/staff/).

## Goal

This code is traning two models to classify an image into one of the following groups:
* Daisy
* Dandelion
* Rose
* Sunflower
* Tulip


## Data

This code is trained on the images from the following Datasets:
* https://www.kaggle.com/mgornergoogle/five-flowers
* https://www.kaggle.com/ianmoone0617/flower-goggle-tpu-classification


## Training the model

The training was processed with [Google Colab](https://colab.research.google.com/drive/1xMJ1Kt4YBeIpqGIzPt1Km8ziwNW5a2Og) to take advantage of the fast computation with GPU.

## Results

![Different Flowers and predictions](https://gitlab.com/ganymede/ganymede.gitlab.io/-/raw/master/source/_posts/flowers.png?inline=false)

## Code

The source code is released under the [MIT License](https://github.com/mansurova/flowers/blob/master/LICENSE) on GitHub:  
https://github.com/mansurova/flowers

### Report and Presentation

The [report](https://gitlab.com/ganymede/flowers-report) (submitted: 26.06.2020)  and [presentation files](https://gitlab.com/ganymede/flowers-presentation) (presented: 14.07.2020) are also available under CC-BY and MIT License.
