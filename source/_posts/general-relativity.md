---
title: General Relativity Theory
date: 2020-07-17
tags: [Physics, Simulation]
---

I have created this project for the german lecture on [general relativity theory](https://itp.uni-frankfurt.de/~hanauske/VARTC/) by [Dr. Matthias Hanauske](https://itp.uni-frankfurt.de/~hanauske/).

It can solve the geodaetic equations of a single body moving around a black hole.


## Example
<video width="87%" controls>
  <source src="https://gitlab.com/ganymede/general-relativity/-/jobs/645314694/artifacts/raw/geodaetic_solution.mp4" type="video/mp4">
  Your browser does not support this video.
</video> 

![Geodaetic solution to an initial values problem](https://gitlab.com/ganymede/general-relativity/-/jobs/645314694/artifacts/raw/geodaetic_solution.png)


## Code

The source code for the general relativity project is available in the following GitLab repository:  
https://gitlab.com/ganymede/general-relativity