---
title: Machine Learning I
date: 2020-07-09
tags: [IT, Machine Learning]
---

I have uploaded my summary of several different machine learning courses by [Dr. Eric Hilebrandt](https://www.uni-frankfurt.de/46992247/Das_Team_von_Prof__Dr__Hartmut_Roskos), [Prof. Rudolf Mester](https://www.vsi.cs.uni-frankfurt.de/), [Prof. Matthias Kaschube](https://www.fias.science/en/neuroscience/research-groups/matthias-kaschube/), [Prof. Nils Bertschinger](https://www.fias.science/de/systemische-risiken/gruppen/nils-bertschinger/), [Prof. Gemma Roig](http://www.cvai.cs.uni-frankfurt.de/) and [Prof. Visvanathan Ramesh](http://www.ccc.cs.uni-frankfurt.de/):

* [Machine Learning Cheat Sheet](https://gitlab.com/ganymede/machine-learning/-/jobs/629581229/artifacts/raw/cheat-sheet.pdf)
* [Machine Learning Mindmap](https://gitlab.com/ganymede/machine-learning/-/raw/master/ML1_Mindmap.pdf?inline=false)

https://gitlab.com/ganymede/machine-learning