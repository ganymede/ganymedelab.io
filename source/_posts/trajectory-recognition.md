---
title: Trajectory Recognition Project
date: 2020-07-16
tags: [IT, Machine Learning, Image Recognition, Video]
---

I have created the following project for the course "Grundlagen der computergestützten Signalverarbeitung" by [Dr.-Ing Jochen Moll](https://www.uni-frankfurt.de/47600283/Goethe_Leibniz_Terahertz_Center).

The goal is to read a video with python and recognize moving objects like birds or insects and their trajectory. The output is a picture of the background with the trajectory on top.


## Example

![Multiple Birds with DBSCAN](https://gitlab.com/ganymede/trajectory-recognition/-/jobs/618034232/artifacts/raw/output/img/multi-bird1-FrameDifference-DBSCAN-2-4.jpg)

![Single Bird 1](https://gitlab.com/ganymede/trajectory-recognition/-/jobs/618034232/artifacts/raw/output/img/vid3-WeightedMovingMean-DBSCAN-10-4.jpg)

![Single Bird 2](https://gitlab.com/ganymede/trajectory-recognition/-/jobs/618034232/artifacts/raw/output/img/vid4-FrameDifference-DBSCAN-5-4.jpg)

![Single Insect](https://gitlab.com/ganymede/trajectory-recognition/-/jobs/618034232/artifacts/raw/output/img/vid1-FrameDifference-DBSCAN-10-4.jpg)


## Code

The source code for the trajectory recognition project is available in the following GitLab repository:  
https://gitlab.com/ganymede/trajectory-recognition/

The poster presenting the results can be accessed here:  
https://gitlab.com/ganymede/trajectory-poster/

More details about this Project are described in the Wiki:  
https://gitlab.com/ganymede/trajectory-poster/